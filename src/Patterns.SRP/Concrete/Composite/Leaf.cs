﻿using Patterns.SRP.Abstract.Decorator;

namespace Patterns.SRP.Concrete.Composite
{
    public class Leaf : IComponent
    {
        public void Something() { }
    }
}