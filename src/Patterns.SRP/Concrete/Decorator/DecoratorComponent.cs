﻿using Patterns.SRP.Abstract.Decorator;

namespace Patterns.SRP.Concrete.Decorator
{
    public class DecoratorComponent : IComponent
    {
        public DecoratorComponent(IComponent decoratedComponent)
        {
            this.decoratedComponent = decoratedComponent;
        }

        public void Something()
        {
            SomethingElse();
            decoratedComponent.Something();
        }

        public void SomethingElse() { }

        private readonly IComponent decoratedComponent;
    }
}