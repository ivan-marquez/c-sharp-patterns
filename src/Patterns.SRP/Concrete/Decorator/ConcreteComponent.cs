﻿using Patterns.SRP.Abstract.Decorator;

namespace Patterns.SRP.Concrete.Decorator
{
    public class ConcreteComponent : IComponent
    {
        public void Something() { }
    }
}