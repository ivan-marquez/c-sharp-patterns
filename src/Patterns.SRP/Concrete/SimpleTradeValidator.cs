﻿using log4net;
using Patterns.SRP.Abstract;

namespace Patterns.SRP.Concrete
{
    public class SimpleTradeValidator : ITradeValidator
    {
        private readonly ILog logger;

        public SimpleTradeValidator(ILog logger)
        {
            this.logger = logger;
        }

        public bool Validate(string[] tradeData)
        {
            if (tradeData.Length != 3)
            {
                logger.Warn(string.Format("Line malformed. Only {1} field(s) found.", tradeData.Length));
                return false;
            }

            if (tradeData[0].Length != 6)
            {
                logger.Warn(string.Format("Trade currencies malformed: '{1}'", tradeData[0]));
                return false;
            }

            int tradeAmount;
            if (!int.TryParse(tradeData[1], out tradeAmount))
            {
                logger.Warn(string.Format("Trade amount not a valid integer: '{1}'", tradeData[1]));
                return false;
            }

            decimal tradePrice;
            if (!decimal.TryParse(tradeData[2], out tradePrice))
            {
                logger.Warn(string.Format("WARN: Trade price not a valid decimal: '{1}'", tradeData[2]));
                return false;
            }
            return true;
        }
    }
}