﻿using Patterns.SRP.Abstract.Decorator;
using Patterns.SRP.Abstract.Predicate;

namespace Patterns.SRP.Concrete.Predicate
{
    public class PredicatedComponent : IComponent
    {
        public PredicatedComponent(IComponent decoratedComponent, IPredicate predicate)
        {
            this.decoratedComponent = decoratedComponent;
            this.predicate = predicate;
        }

        public void Something()
        {
            if (predicate.Test())
            {
                decoratedComponent.Something();
            }
        }

        private readonly IComponent decoratedComponent;
        private readonly IPredicate predicate;
    }
}