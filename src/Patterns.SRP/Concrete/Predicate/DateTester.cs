﻿using System;

namespace Patterns.SRP.Concrete.Predicate
{
    public class DateTester
    {
        public bool TodayIsAnEvenDayOfTheMonth
        {
            get
            {
                return (DateTime.Now.Day % 2).Equals(0);
            }
        }
    }
}