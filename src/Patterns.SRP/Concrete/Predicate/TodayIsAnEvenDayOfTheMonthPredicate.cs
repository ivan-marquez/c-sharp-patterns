﻿using Patterns.SRP.Abstract.Predicate;

namespace Patterns.SRP.Concrete.Predicate
{
    public class TodayIsAnEvenDayOfTheMonthPredicate : IPredicate
    {
        public TodayIsAnEvenDayOfTheMonthPredicate(DateTester dateTester)
        {
            this.dateTester = dateTester;
        }

        public bool Test()
        {
            return dateTester.TodayIsAnEvenDayOfTheMonth;
        }

        private readonly DateTester dateTester;
    }
}