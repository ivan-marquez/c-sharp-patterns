﻿namespace Patterns.SRP.Abstract
{
    public interface ITradeValidator
    {
        bool Validate(string[] tradeData);
    }
}