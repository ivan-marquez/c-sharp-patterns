﻿namespace Patterns.SRP.Abstract.Decorator
{
    public interface IComponent
    {
        void Something();
    }
}