﻿namespace Patterns.SRP.Abstract.Predicate
{
    public interface IPredicate
    {
        bool Test();
    }
}