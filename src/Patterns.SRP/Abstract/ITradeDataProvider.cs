﻿using System.Collections.Generic;

namespace Patterns.SRP.Abstract
{
    public interface ITradeDataProvider
    {
        IEnumerable<string> GetTradeData();
    }
}