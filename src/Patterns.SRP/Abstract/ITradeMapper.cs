﻿using Patterns.SRP.Concrete;

namespace Patterns.SRP.Abstract
{
    public interface ITradeMapper
    {
        TradeRecord Map(string[] fields);
    }
}