﻿using Patterns.SRP.Concrete;
using System.Collections.Generic;

namespace Patterns.SRP.Abstract
{
    public interface ITradeParser
    {
        IEnumerable<TradeRecord> Parse(IEnumerable<string> tradeData);
    }
}