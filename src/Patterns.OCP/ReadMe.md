# Open / Close Principle

---

**Open for extension**. This means that the behavior of the module can be extended. As the
requirements of the application change, we are able to extend the module with new behaviors
that satisfy those changes. In other words, we are able to change what the module does.

**Closed for modification**. Extending the behavior of a module does not result in changes to
the source or binary code of the module. The binary executable version of the module, whether
in a linkable library, a DLL, or a Java .jar, remains untouched.

## Exceptions to the Open / Close Principle

### Bug fixes

Bugs are a common problem in software, and they are impossible to prevent entirely. When they do
occur, though, you need to respond by fixing the problem code. Of course, this involves a
modification to an existing class; that is, unless you are willing to duplicate the class and
implement the bug fix on the new version. This sounds needlessly convoluted and runs counter to
the guiding principle of erring on the side of pragmatism rather than purity.

### Client awareness

A more permissive exception to the *closed for modification* rule is that any change is allowed to
existing code as long as it does not also require a change to any client of that code. This places
an emphasis on how coupled the software modules are, at all levels of granularity: between classes
and classes, assemblies and assemblies, and subsystems and subsystems. If a change in one class
forces a change in another, the two are said to be *tightly coupled*. Conversely, if a class can
change in isolation without forcing other classes to change, the participating classes are *loosely
coupled*. At all times and at all levels, loose coupling is preferable. Maintaining loose coupling
limits the impact that the OCP has if you allow modifications to existing code that does not force
further changes to clients. 

## Extension points through Interface Inheritance

**Interface inheritance is preferable to implementation inheritance**. With implementation 
inheritance, all subclasses, present and future, are clients. This prevents modification because,
with implementation inheritance, subclasses depend on the implementation, too. All implementation
changes are thus potentially client-aware changes. This echoes the advice to **prefer composition
over inheritance** and to keep inheritance hierarchies shallow, with few layers of subclassing. If
a change is made to add a member at the top of the inheritance graph, that change affects all
members of the hierarchy. 

Interfaces are also better extension points because they can be decorated with rich object graphs
of functionality that calls upon many different contexts. They are more flexible than classes.
I don't mean that the virtual and abstract methods that form the extension points of class
inheritance are not useful, but that they do not provide quite the same level of adaptability that
interfaces do.

### Design for inheritance or prohibit it

If you choose to use implementation inheritance as an extension point, you must design and document
the class properly so as to protect and inform future programmers who extend the class. Inheritance
of classes can be tricky; new subclasses can break existing code in unpredictable ways. 

It is very important to note that any class that is not marked with the **sealed** keyword claims
to support inheritance. A class does not need to be abstract or to contain virtual methods in order
to be subclassed. The `new` keyword can be used to hide inherited members, but this blocks
polymorphism, possibly defying expectations.

### Protected variation

***Identify points of predicted variation and create a stable interface around them.***

The requirements of an individual class should be linked directly to a business client's
requirement. If this link is ignored, there is a risk that the class will not serve any purpose
that the business client requested. Over the course of a sprint, user stories are taken from the
sprint backlog, and developers and the product owner converse. At this point, questions should be
asked as to the potential for future, related requirements. This informs the predicted variation
that can be translated into extension points.

### A stable interface

Even if you delegate only to interfaces, clients are still dependent on those interfaces. *If the
interface changes, the client must also change*. A key advantage of depending on interfaces is that
they are much less likely to change than implementations. If you place the interface in a separate
assembly from its implementation, as the Stairway pattern suggests, the two can vary without
affecting each other, and the implementation can change without affecting clients. 

Clearly, it is very important that **all interfaces chosen to represent an extension point should
be stable**. The likelihood and frequency of interface changes should be low, otherwise you will
need to amend all clients to use the new version.

## Conclusion

The open/closed principle is a guideline for the overall design of classes and interfaces and how
developers can build code that allows change over time. With each passing sprint, new requirements
are inevitable and should be embraced. Acknowledging that change is a good thing is only part of
the answer, however. If the code you have produced up until this point is not built to enable
change, change will be difficult, time consuming, error prone, and costly. By ensuring that your
code is open to extension but closed to modification, you effectively disallow future changes to
existing classes and assemblies, which forces programmers to create new classes that can plug into
the extension points. 

There are two main types of extension point available: implementation inheritance and interface
inheritance. Virtual and abstract methods allow you to cre-ate subclasses that customize methods
in a base class. If classes delegate to interfaces, this provides you with more flexibility in your
extension points by virtue of a variety of patterns. 

Knowing that you can integrate extension points into code is not sufficient, however. You also need
to know when this is applicable. The concept of protected variation suggests that you identify
parts of the requirements that are likely to change or that are particularly troublesome to
implement, and factor these out behind extension points. 

Code can be quite rigidly defined, with little scope for extension or elaboration, or it can be
very fluid, with myriad extension points ready to handle new requirements. Either of these options
can be correct, depending on the specific scenario and context.