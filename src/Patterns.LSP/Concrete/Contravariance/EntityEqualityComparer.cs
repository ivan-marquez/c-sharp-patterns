﻿using Patterns.LSP.Concrete.Covariance;
using contravariance = Patterns.LSP.Abstract.Contravariance;

namespace Patterns.LSP.Concrete.Contravariance
{
    public class EntityEqualityComparer : contravariance.IEqualityComparer<Entity>
    {
        public bool Equals(Entity left, Entity right)
        {
            return left.ID == right.ID;
        }
    }
}