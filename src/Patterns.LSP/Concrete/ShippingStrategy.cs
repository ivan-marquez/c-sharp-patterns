﻿using System;
using System.Linq;
using System.Globalization;
using Patterns.LSP.Abstract;
using System.Collections.Generic;

namespace Patterns.LSP.Concrete
{
    public class ShippingStrategy : IShippingStrategy
    {
        /// <summary>
        /// Data Invariants:
        /// A data invariant is a predicate that remains true for the lifetime of an object; it is true after construction and must remain true 
        /// until the object is out of scope. Data invariants relate to the expected internal state of the object.
        /// </summary>
        public ShippingStrategy(decimal flatRate)
        {
            if (flatRate <= decimal.Zero)
                throw new ArgumentOutOfRangeException("flatRate", "Flat rate must be positive and nonzero");

            this.flatRate = flatRate;
        }

        /// <summary>       
        /// Preconditions:        
        /// Preconditions are defined as all of the conditions necessary for a method to run reliably and without fault. Every 
        /// method requires some preconditions to be true before it should be called. By default, interfaces force no guarantees 
        /// on any of the implementers of their methods.        
        /// 
        /// Postconditions:
        /// Postconditionscheck whether an object is being left in a valid state as a method is exited. Whenever state is mutated in a 
        /// method, it is possible for the state to be invalid due to logic errors. Postconditions are implemented in the same manner 
        /// as preconditions, through guard clauses. How-ever, rather than placing the clauses at the start of the method, postcondition 
        /// guard clauses must be placed at the end of the method after all edits to state have been made.
        /// </summary>
        public virtual decimal CalculateShippingCost(float packageWeightInKilograms, IEnumerable<float> packageDimensionsInInches,
            RegionInfo destination)
        {
            /// <summary>
            /// With these preconditions in place, clients must ensure that the parameters that they provide are within valid ranges before calling. 
            /// One corollary from this is that all of the state that is checked in a precondition must be publically accessible by clients. If the 
            /// client is unable to verify that the method they are about to call will throw an error due to an invalid precondition, the client won’t 
            /// be able to ensure that the call will succeed. Therefore, private state should not be the target of a precondition; only method parameters 
            /// and the class’s public properties should have preconditions.
            /// </summary>
            if (packageWeightInKilograms <= 0f)
                throw new ArgumentOutOfRangeException("packageWeightInKilograms",
                    "Package weight must be positive and non-zero");

            if (packageDimensionsInInches.First() <= 0f)
                throw new ArgumentOutOfRangeException("packageDimensionsInInches",
                    "Package dimensions must be positive and non-zero");

            var shippingCost = decimal.MinusOne;

            /// <summary>
            /// By testing state against a predetermined valid range, and throwing an exception if the value falls outside of that range, you 
            /// can enforce a postcondition on the method. The postcondition here relates not to the state of the object but to the return 
            /// value. Much like method argument values are tested against preconditions for validity, so are method return values tested 
            /// against postconditions for validity.
            /// </summary>
            if (shippingCost <= decimal.Zero)
                throw new ArgumentOutOfRangeException("return", "The return value is out of range");

            return decimal.MinusOne;
        }

        protected decimal flatRate;

        /// <summary>
        /// Data Invariants (cont.):
        /// However, if the flatRate variable is instead a publically settable property, the guard clause would have to be moved to the setter 
        /// block in order to protect the data invariant. 
        /// </summary>
        public decimal FlatRate
        {
            get
            {
                return flatRate;
            }
            set
            {
                if (value <= decimal.Zero)
                    throw new ArgumentOutOfRangeException("value", "Flat rate must be positive and nonzero");
                flatRate = value;
            }
        }
    }
    /// <summary>
    /// Encapsulation vs. contracts:
    /// The contracts implemented in this example make sense, but they are caused by a poor choice of types for each value. The precondition 
    /// contract for ensuring that the package weight argument is non-zero and positive is intrinsically linked with the type of the variable: weight 
    /// should never be zero or negative. THIS MAKES WEIGHT A CANDIDATE FOR ENCAPSULATION INTO ITS OWN TYPE. If, as is likely, another class or method 
    /// requires a weight, you would need to carry this precondition across to the new code. This is inefficient, hard to maintain, and error-prone. It 
    /// makes more sense to create a new type and define the precondition with it so that all uses of the Weighttype must have a non-zero and positive value. 
    /// It is, in fact, an invariant of the type rather than a precondition of the CalculateShippingCost method.
    /// </summary>
}