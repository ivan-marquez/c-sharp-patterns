﻿using System;

namespace Patterns.LSP.Concrete.Covariance
{
    public class User : Entity
    {
        public string EmailAddress { get; private set; }
        public DateTime DateOfBirth { get; private set; }
    }
}