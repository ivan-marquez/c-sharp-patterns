﻿using System;

namespace Patterns.LSP.Concrete.Covariance
{
    public class Entity
    {
        public Guid ID { get; private set; }
        public string Name { get; private set; }
    }
}