﻿using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Patterns.LSP.Concrete.Contracts
{
    /// <summary>
    /// Code contracts:
    /// Previously a separate library, code contracts were integrated into the .NET Framework 4.0 main libraries. In 
    /// addition to being easier to read, write, and comprehend than manual guard clauses, code contracts bring with 
    /// them the possibility of using static verification and automatic generation of reference documentation. With 
    /// static contract verification, code contracts are able to check for contract violations without executing the 
    /// application. This helps expose implicit contracts such as null dereferences and problems with array bounds, 
    /// in addition to the explicitly coded contracts shown throughout this section.
    /// </summary>
    public class ShippingStrategy
    {
        public ShippingStrategy(decimal flatRate)
        {
            this.flatRate = flatRate;
        }

        /// <summary>
        /// Data invariants
        /// It is common for each method in a class to contain its own preconditions and postconditions, but data invariants relate to 
        /// the class as a whole. Code contracts allow you to create a private method on the class that contains declarative definitions 
        /// of the class’s invariants. 
        /// 
        /// If this were a normal private method, you would be obliged to call the method at the start and end of every method, to ensure 
        /// that the invariants were correctly protected. Luckily, you can have code contracts do this on your behalf by marking the method 
        /// with the ContractInvariantMethod­Attribute. Remember that attributes do not require the Attribute suffix, so this has been shortened 
        /// in the example to ContractInvariantMethod. This flags the method as one that code contracts must call when entering and leaving a 
        /// method, to confirm that the class’s data invariants are not being violated. The prerequisites for marking a method as a 
        /// Contract Invariant Method are that it must return void and accept no arguments. However, it can be public or private, and you can 
        /// choose any name to describe the method. Classes can have more than one Contract Invariant Method, so logically grouping them is also 
        /// possible. The body of the method must only make calls to the Contract.Invariant method.
        /// </summary>
        [ContractInvariantMethod]
        private void ClassInvariant()
        {
            Contract.Invariant(flatRate > 0m, "Flat rate must be positive and nonzero");
        }

        public decimal CalculateShippingCost(float packageWeightInKilograms,
            IEnumerable<float> packageDimensionsInInches, RegionInfo destination)
        {
            /// <summary>
            /// Preconditions:
            /// The Contract.Requires method accepts a Boolean predicate value. This represents the state that the method requires 
            /// in order to proceed. Note that this is the exact opposite of the predi-cate used in an if statement in manual guard 
            /// clauses. In that case, the clauses were checking for state that was invalid before throwing an exception. With code 
            /// contracts, the predicate is closer to an assertion: that the Boolean value must return true, otherwise the 
            /// contract fails. 
            /// </summary>
            Contract.Requires<ArgumentOutOfRangeException>(packageWeightInKilograms > 0f,
                "Package weight must be positive and nonzero.");

            Contract.Requires<ArgumentOutOfRangeException>(packageDimensionsInInches.First() > 0f,
                "Package dimensions must be positive and nonzero");

            /// <summary>
            /// Postconditions:
            /// Code contracts can similarly provide a shortcut to defining postconditions. The Contract static class contains an Ensures 
            /// method that is the postcondition complement to the precondition’s Requiresmethod. This method also accepts a Boolean predicate 
            /// that must be true in order to progress through to the return statement. It is worth noting that the return statement must be 
            /// the only line that follows a call to Contract.Ensures. This makes intuitive sense because, otherwise, it would be possible to 
            /// further modify state in a way that might break the postcondition.
            /// </summary>
            Contract.Ensures(Contract.Result<decimal>() > 0m);

            return decimal.MinusOne;
        }

        protected decimal flatRate;
    }
}