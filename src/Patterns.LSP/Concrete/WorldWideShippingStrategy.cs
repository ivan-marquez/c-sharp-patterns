﻿using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;

namespace Patterns.LSP.Concrete
{
    public class WorldWideShippingStrategy : ShippingStrategy
    {
        /// <summary>
        /// Invariants must be maintained:
        /// Whenever a new subclass is created, it must continue to honor all of the data invariants that were part of 
        /// the base class. This is an easy problem to introduce because subclasses have a lot of freedom to introduce 
        /// new ways of changing previously private data.
        /// </summary>
        public WorldWideShippingStrategy(decimal flatRate) : base(flatRate) { }

        public override decimal CalculateShippingCost(float packageWeightInKilograms,
            IEnumerable<float> packageDimensionsInInches, RegionInfo destination)
        {
            if (packageWeightInKilograms <= 0f)
                throw new ArgumentOutOfRangeException("packageWeightInKilograms", "Package weight must be positive and nonzero");

            if (packageDimensionsInInches.First() <= 0f || packageDimensionsInInches.All(x => x <= 0f))
                throw new ArgumentOutOfRangeException("packageDimensionsInInches", "Package dimensions must be positive and nonzero");

            /// <summary>
            /// Preconditions cannot be strengthened:
            /// Whenever a subclass overrides an existing method that contains preconditions, it must never strengthen the 
            /// existing preconditions. Doing so would potentially break any client code that already assumes that the 
            /// superclass defines the strongest possible precondition contracts for any method.
            /// 
            /// The temptation is to strengthen the preconditions so that you can guarantee that the destination parameter is 
            /// provided. This creates a conflict that calling code is unable to solve. If a class calls the CalculateShippingCost 
            /// method of the ShippingStrategy class, it is free to pass in a null value for the destination parameter without 
            /// experiencing a side effect. But if it is calling the CalculateShippingCost method of the WorldWideShippingStrategy 
            /// class, it must not pass in a null value for the destination parameter. Doing so would violate a precondition and cause 
            /// an exception to be thrown. As earlier chapters have demonstrated, client code must never make assumptions about what 
            /// type it is acting on. Doing so only leads to strong coupling between classes and an inability to adapt to changes in 
            /// requirements.
            /// </summary>
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination must be provided");

            var shippingCost = decimal.One;

            /// <summary>
            /// Postconditions cannot be weakenedWhen applying postconditions to subclasses, the opposite rule applies. Instead of not 
            /// being able to strengthen postconditions, you cannot weaken them. As for all of the Liskov substitution rules relating 
            /// to contracts, the reason that you cannot weaken postconditions is because existing clients might break when presented 
            /// with the new subclass. Theoretically, if you comply with the LSP, any subclass you create should be usable by all 
            /// existing clients without causing them to fail in unexpected ways.
            /// 
            /// A client could easily be broken by this change in behavior due to its assumption of the value of the shipping cost. For 
            /// example, the client assumes that the shipping cost is always positive and non-zero, as indicated by the postcondition 
            /// contract of the ShippingStrategy. This client then uses the shipping cost as the denominator in a subsequent calculation.
            /// When a switch is made to use the new WorldWideShippingStrategy, the client unexpectedly starts throwing 
            /// DivideByZeroException errors for all domestic orders. Had the LSP been honored and the postcondition never weakened, this 
            /// defect would never have been introduced.
            /// </summary>
            if (destination == RegionInfo.CurrentRegion)
                shippingCost = decimal.Zero;

            return shippingCost;
        }

        /// <summary>
        /// Invariants must be maintained (cont.):
        /// When the base class is reworked to disallow direct write access to the flat rate field, the invariant is properly honored 
        /// by the subclass. This is a very common pattern whereby fields are private but have protected or public properties that 
        /// contain guard clauses to protect the invariants.
        /// </summary>
        public new decimal FlatRate { get { return base.FlatRate; } set { base.FlatRate = value; } }
    }
}