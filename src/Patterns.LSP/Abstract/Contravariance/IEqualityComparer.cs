﻿using Patterns.LSP.Concrete.Covariance;

namespace Patterns.LSP.Abstract.Contravariance
{
    /// <summary>
    /// Contravariance enables you to use a less derived type than that specified by the generic parameter. This allows for implicit 
    /// conversion of classes that implement variant interfaces and implicit conversion of delegate types. Covariance and contravariance 
    /// in generic type parameters are supported for reference types, but they are not supported for value types.
    /// 
    /// A type can be declared contravariant in a generic interface or delegate if it is used only as a type of method arguments and not 
    /// used as a method return type.Ref and out parameters cannot be variant.
    /// 
    /// An interface that has a contravariant type parameter allows its methods to accept arguments of less derived types than those 
    /// specified by the interface type parameter.For example, because in .NET Framework 4, in the IComparer<T> interface, type T is 
    /// contravariant, you can assign an object of the IComparer(Of Person) type to an object of the IComparer(Of Employee) type 
    /// without using any special conversion methods if Employee inherits Person.
    ///
    /// A contravariant delegate can be assigned another delegate of the same type, but with a less derived generic type parameter.
    /// </summary>
    public interface IEqualityComparer<in TEntity> where TEntity : Entity
    {
        bool Equals(TEntity left, TEntity right);
    }
    /// <summary>
    /// There would be no type conversion from EntityEqualityComparer to IEqualityComparer­<User>, which is intuitive because Entity is 
    /// the supertype and User is the subtype. However, because the IEqualityComparer supports contravariance, the existing inheritance 
    /// hierarchy is inverted and you are able to assign what was originally a less specific type to a more specific type via the 
    /// IEqualityComparer interface.
    /// </summary>
}