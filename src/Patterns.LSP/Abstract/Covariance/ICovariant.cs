﻿namespace Patterns.LSP.Abstract.Covariance
{
    public interface ICovariant<out T>
    {
        T MethodWhichReturnsT();
    }
}