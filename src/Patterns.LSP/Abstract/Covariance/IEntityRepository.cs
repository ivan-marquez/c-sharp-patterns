﻿using System;
using Patterns.LSP.Concrete.Covariance;

namespace Patterns.LSP.Abstract.Covariance
{
    /// <summary>
    /// For generic type parameters, the out keyword specifies that the type parameter is covariant. You can use the out keyword 
    /// in generic interfaces and delegates.
    /// 
    /// Covariance enables you to use a more derived type than that specified by the generic parameter.This allows for implicit conversion 
    /// of classes that implement variant interfaces and implicit conversion of delegate types. Covariance and contravariance are 
    /// supported for reference types, but they are not supported for value types.
    /// 
    /// An interface that has a covariant type parameter enables its methods to return more derived types than those specified by the type 
    /// parameter. For example, because in .NET Framework 4, in IEnumerable<T>, type T is covariant, you can assign an object of the 
    /// IEnumerabe(Of String) type to an object of the IEnumerable(Of Object) type without using any special conversion methods.
    /// </summary>    
    public interface IEntityRepository<out TEntity> where TEntity : Entity
    {
        TEntity GetByID(Guid id);
    }
}