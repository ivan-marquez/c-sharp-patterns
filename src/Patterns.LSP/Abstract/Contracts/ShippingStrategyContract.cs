﻿using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Patterns.LSP.Abstract.Contracts
{
    /// <summary>
    /// When writing an interface contract, you also need a class that is going to implement the methods of the interface but only fill them 
    /// with uses of the Contract.Requires and Contract.Ensuresmethods. The abstract ShippingStrategyContract provides this functionality and 
    /// looks like the prior examples, but what the prior examples lacked was the real functionality of the method. Even in production code, 
    /// this is the limit of the code contained in a contract class. There is also a ContractInvariantMethod to house any calls to 
    /// Contract.Invariant, just as if this class were the real implementation.
    /// </summary>
    [ContractClassFor(typeof(IShippingStrategy))]
    public abstract class ShippingStrategyContract : IShippingStrategy
    {
        public decimal CalculateShippingCost(float packageWeightInKilograms,
            IEnumerable<float> packageDimensionsInInches, RegionInfo destination)
        {
            Contract.Requires<ArgumentOutOfRangeException>(packageWeightInKilograms > 0f,
                "Package weight must be positive and nonzero");

            Contract.Requires<ArgumentOutOfRangeException>(packageDimensionsInInches.First() > 0f,
                "Package dimensions must be positive and nonzero");

            Contract.Ensures(Contract.Result<decimal>() > 0m); return decimal.One;
        }

        [ContractInvariantMethod]
        private void ClassInvariant()
        {
            Contract.Invariant(flatRate > 0m, "Flat rate must be positive and nonzero");
        }

        private decimal flatRate;
    }
    /// <summary>
    /// This concludes the introduction to code contracts and the foray into the Liskov substitution principle’s rules relating to contracts. 
    /// One final important point needs to be emphasized. Whether they are implemented manually or by using code contracts, if a precondition, 
    /// postcondition, or invariant fails, CLIENTS SHOULD NOT CATCH THE EXCEPTION. Catching an exception is an action that indicates that the 
    /// client can recover from this situation, which is seldom likely or perhaps even possible when a contract is broken. The ideal is that 
    /// all contract violations will happen during functional testing and that the offending code will be fixed before shipping. This is why 
    /// it is so important to unit test contracts. If a contract violation is not fixed before shipping and an end user is unfortunate enough 
    /// to trigger an exception, it is most likely the best course of action to force the application to close. It is advisable to allow the 
    /// application to fail because it is now in a potentially invalid state. For a web application, this will mean that the global error page 
    /// is displayed. For a desktop application, the user can be shown a friendly message and be given a chance to report the problem. In any 
    /// and all cases, A LOG SHOULD BE MADE OF THE EXCEPTION, with full stack trace and as much context as possible.
    /// </summary>
}