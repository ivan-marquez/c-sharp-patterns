﻿using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Patterns.LSP.Abstract.Contracts
{
    /// <summary>
    /// To link the interface to the contract class implementation, you unfortunately need a two-way reference via an attribute. This is 
    /// somewhat unfortunate because it adds noise to the interface, which itwould be nice to avoid. Nevertheless, by marking the interface 
    /// with the ContractClass attribute and the contract class with the ContractClassFor attribute, you can write your preconditions, 
    /// postconditions, and data invariant protection code once and have it apply to all subsequent implementations of the interface. Both the 
    /// ContractClass and ContractClassFor attributes accept a Type argument. The ContractClass is applied to the interface and has the contract 
    /// class type passed in, whereas the ContractClassFor is applied to the contract class and has the interface type passed in.
    /// </summary>
    [ContractClass(typeof(ShippingStrategyContract))]
    public interface IShippingStrategy
    {
        decimal CalculateShippingCost(float packageWeightInKilograms, 
            IEnumerable<float> packageDimensionsInInches, RegionInfo destination);
    }
}