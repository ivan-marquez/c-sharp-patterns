﻿using System.Globalization;
using System.Collections.Generic;

namespace Patterns.LSP.Abstract
{
    /// <summary>
    /// Contracts:
    /// 
    /// It is often said that developers should program to interfaces, and a related idiom is to program to a contract. However, beyond 
    /// the apparent method signatures, interfaces convey a very loose notion of a contract. A method signature reveals little about the 
    /// actual requirements and guarantees of the method’s implementation.
    /// 
    /// In a strongly typed language like C#, there is at least a notion of passing the correct type for an argument, but this is largely 
    /// where the interface ends and the concept of the contract must begin.
    /// </summary>
    public interface IShippingStrategy
    {
        decimal CalculateShippingCost(float packageWeightInKilograms, IEnumerable<float> packageDimensionsInInches, 
            RegionInfo destination);
    }
}