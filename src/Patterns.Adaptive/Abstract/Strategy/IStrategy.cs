﻿
namespace Patterns.Adaptive.Abstract.Strategy
{
    public interface IStrategy
    {
        void Execute();
    }
}