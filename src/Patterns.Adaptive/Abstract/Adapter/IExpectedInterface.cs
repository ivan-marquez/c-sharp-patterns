﻿namespace Patterns.Adaptive.Abstract.Adapter
{
    public interface IExpectedInterface
    {
        void MethodA();
    }
}