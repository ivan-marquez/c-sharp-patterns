﻿using System;

namespace Patterns.Adaptive.Abstract.NullObject
{
    public interface IUserRepository
    {
        IUser GetById(Guid userId);
    }
}