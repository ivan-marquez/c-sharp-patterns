﻿using System;

namespace Patterns.Adaptive.Abstract.NullObject
{
    public interface IUser
    {
        Guid UserId { get; set; }

        string Name { get; set; }

        void IncrementSessionTicket();
    }
}