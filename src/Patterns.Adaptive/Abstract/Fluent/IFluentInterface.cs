﻿namespace Patterns.Adaptive.Abstract.Fluent
{
    public interface IFluentInterface
    {
        IFluentInterface DoSomething();

        IFluentInterface DoSomethingElse();

        void ThisMethodIsNotFluent();
    }
}