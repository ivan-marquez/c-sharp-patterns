﻿using System;
using Patterns.Adaptive.Abstract.Strategy;

namespace Patterns.Adaptive.Concrete.Strategy
{
    public class ConcreteStrategyA : IStrategy
    {
        public void Execute()
        {
            Console.WriteLine("ConcreteStrategyA.Execute()");
        }
    }
}