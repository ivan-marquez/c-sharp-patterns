﻿using Patterns.Adaptive.Abstract.Strategy;

namespace Patterns.Adaptive.Concrete.Strategy
{
    public class Context
    {
        public Context()
        {
            currentStrategy = strategyA;
        }

        public void DoSomething()
        {
            currentStrategy.Execute();

            //swap strategy with each call
            currentStrategy = (currentStrategy == strategyA) ? strategyA : strategyB;
        }

        private readonly IStrategy strategyA = new ConcreteStrategyA();
        private readonly IStrategy strategyB = new ConcreteStrategyB();

        private IStrategy currentStrategy;

        /// <summary>
        /// With every call that is made to Context.DoSomething(), the method first delegates to the current 
        /// strategy and then swaps between strategy A and strategy B. The next call delegates to the newly 
        /// selected strategy before again swapping back to the original strategy. 
        /// 
        /// Note:
        /// The way the strategies are selected is an implementation detail. It does not alter the net effect 
        /// of the pattern: that the behavior of the class is hidden behind an interface whose implementations 
        /// are used to perform the real work. 
        /// </summary>
    }
}