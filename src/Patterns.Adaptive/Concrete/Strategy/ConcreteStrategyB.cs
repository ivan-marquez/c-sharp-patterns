﻿using System;
using Patterns.Adaptive.Abstract.Strategy;

namespace Patterns.Adaptive.Concrete.Strategy
{
    public class ConcreteStrategyB : IStrategy
    {
        public void Execute()
        {
            Console.WriteLine("ConcreteStrategyB.Execute()");
        }
    }
}