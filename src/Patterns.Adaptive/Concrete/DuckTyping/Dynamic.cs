﻿namespace Patterns.Adaptive.Concrete.DuckTyping
{
    public class Dynamic
    {
        public static void DoDuckLikeThings(dynamic duckish)
        {
            if(duckish != null)
            {
                duckish.Walk();
                duckish.Swim();
                duckish.Quack();
            }
        }
    }
}