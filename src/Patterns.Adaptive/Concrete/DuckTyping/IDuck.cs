﻿namespace Patterns.Adaptive.Concrete.DuckTyping
{
    public interface IDuck
    {
        void Walk();

        void Swim();

        void Quack();
    }
}