﻿using System;

namespace Patterns.Adaptive.Concrete.DuckTyping
{
    public class Swan
    {
        public void Walk()
        {
            Console.WriteLine("The swan in walking.");
        }

        public void Swim()
        {
            Console.WriteLine("The swan in swimming.");
        }

        public void Quack()
        {
            Console.WriteLine("The swan in quacking.");
        }
    }
}