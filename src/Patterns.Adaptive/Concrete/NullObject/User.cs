﻿using System;
using Patterns.Adaptive.Abstract.NullObject;

namespace Patterns.Adaptive.Concrete.NullObject
{
    public class User : IUser
    {
        public User(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public void IncrementSessionTicket()
        {
            Console.WriteLine("Inside IncrementSessionTicket()");
        }
    }
}