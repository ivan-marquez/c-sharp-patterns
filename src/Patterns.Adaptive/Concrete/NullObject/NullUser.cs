﻿using System;
using Patterns.Adaptive.Abstract.NullObject;

namespace Patterns.Adaptive.Concrete.NullObject
{
    public class NullUser : IUser
    {
        public Guid UserId { get { return Guid.NewGuid(); } set { UserId = Guid.NewGuid(); } }

        public string Name { get { return "unknown"; } set { Name = "unknown"; } }

        public void IncrementSessionTicket()
        {
            //Do nothing.
        }
    }
}