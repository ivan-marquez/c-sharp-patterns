﻿using System;
using System.Linq;
using System.Collections.Generic;
using Patterns.Adaptive.Abstract.NullObject;

namespace Patterns.Adaptive.Concrete.NullObject
{
    /// <summary>
    /// The Null Object pattern indicates that you are placing too much unnecessary burden on all of the clients of IUserRepository. 
    /// The more clients that use this method, the greater the probability of forgetting a null reference check. Instead, you 
    /// should change the source of the problem to perform the check for you.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        public UserRepository()
        {
            users = new List<User>
            {
                new User(Guid.NewGuid()),
                new User(Guid.NewGuid()),
                new User(Guid.NewGuid())
            };
        }

        public IUser GetById(Guid userId)
        {
            IUser userFound = users.SingleOrDefault(user => user.UserId == userId);
            if (userFound == null)
                userFound = new NullUser();

            return userFound;
        }

        private ICollection<User> users;
    }
}