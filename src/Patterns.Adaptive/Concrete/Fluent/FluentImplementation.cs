﻿using System;
using Patterns.Adaptive.Abstract.Fluent;

namespace Patterns.Adaptive.Concrete.Fluent
{
    /// <summary>
    /// Fluent interfaces are easy to implement, too. All the class has to do is return this from 
    /// the method. Because the class is already an implementation of the interface, by returning this, the class returns 
    /// only the interface portion of itself, thus hiding the rest of the implementation.
    /// </summary>
    public class FluentImplementation : IFluentInterface
    {
        public IFluentInterface DoSomething()
        {
            return this;
        }

        public IFluentInterface DoSomethingElse()
        {
            return this;
        }

        public void ThisMethodIsNotFluent()
        {
            Console.WriteLine("Inside ThisMethodIsNotFluent()");
        }
    }
}