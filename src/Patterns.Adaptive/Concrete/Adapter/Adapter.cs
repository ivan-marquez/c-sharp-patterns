﻿using Patterns.Adaptive.Abstract.Adapter;

namespace Patterns.Adaptive.Concrete.Adapter
{
    public class Adapter : IExpectedInterface
    {
        public Adapter(TargetClass target)
        {
            this.target = target;
        }

        public void MethodA()
        {
            target.MethodB();
        }

        private TargetClass target;
    }
}