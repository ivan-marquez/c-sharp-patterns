﻿using System;
using Patterns.SRP.Abstract.Decorator;
using Patterns.SRP.Abstract.Predicate;
using Patterns.SRP.Concrete.Composite;
using Patterns.SRP.Concrete.Decorator;
using Patterns.SRP.Concrete.Predicate;
using Patterns.Adaptive.Abstract.Fluent;
using Patterns.Adaptive.Concrete.Fluent;
using Patterns.Adaptive.Abstract.Adapter;
using Patterns.Adaptive.Concrete.Adapter;
using Patterns.Adaptive.Concrete.Strategy;
using Patterns.Adaptive.Concrete.DuckTyping;
using Patterns.Adaptive.Abstract.NullObject;
using Patterns.Adaptive.Concrete.NullObject;

namespace CSharpPatterns.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /// <summary>
            /// Decorator Pattern
            /// The Decorator pattern’s basic premise is that each decorator class fulfills the contract 
            /// of a type and also accepts one or more of those types as constructor parameters. This is 
            /// beneficial because functionality can be added to an existing class that implements a certain 
            /// interface, and the decorator also acts—unbeknownst to clients—as an implementation of the 
            /// required interface. 
            /// </summary>
            component = new DecoratorComponent(new ConcreteComponent());
            component.Something();

            /// <summary>
            /// Composite Pattern
            /// The Composite pattern’s purpose is to allow you to treat many instances of an 
            /// interface as if they were just one instance.
            /// NOTE: the instances need not all be of the same concrete type.
            /// They just have to implement the interface.
            /// </summary>
            composite = new CompositeComponent();
            composite.AddComponent(new Leaf());
            composite.AddComponent(new Leaf());
            composite.AddComponent(new Leaf());

            component = composite;
            component.Something();

            /// <summary>
            /// Predicate decorators
            /// The predicate decorator is a useful construct for hiding the conditional 
            /// execution of code from clients. 
            /// </summary>
            dateTester = new DateTester();
            predicate = new TodayIsAnEvenDayOfTheMonthPredicate(dateTester);
            var predicatedComponent = new PredicatedComponent(component, predicate);

            predicatedComponent.Something();

            /// <summary>
            /// The Adapter pattern allows you to provide an object instance to a client that has a dependency on an interface that 
            /// your instance does not implement. An Adapter class is created that fulfills the expected interface of the client but 
            /// that implements the methods of the interface by delegating to different methods of another object. It is typically 
            /// used when the target class cannot be altered to fit the desired interface. This could be because it is sealed or because 
            /// it belongs to an assembly for which you do not have the source.
            /// </summary>
            dependency = new Adapter(new TargetClass());
            dependency.MethodA();

            /// <summary>
            /// The Strategy pattern allows you to change the desired behavior of a class without requiring recompilation, potentially 
            /// even during run-time execution. 
            /// 
            /// The Strategy pattern is used whenever a class needs to exhibit variant behavior depending on the state of an object. If 
            /// this behavior can change at run time depending on the current state of the class, the Strategy pattern is a perfect fit 
            /// for encapsulating this variant behavior.
            /// </summary>
            context = new Context();
            context.DoSomething();

            /// <summary>
            ///  With the introduction of the dynamickeyword, and some supporting types, you can avoid the CLR’s static typing and switch 
            ///  to the dynamic typing of the Dynamic Language Runtime (DLR).
            /// </summary>
            swan = new Swan();
            Dynamic.DoDuckLikeThings(swan);

            /// <summary>
            /// The Null Object pattern is one of the most common design patterns—and it is one of the easiest to comprehend and implement. 
            /// Its purpose is to allow you to avoid accidentally throwing a Null­ReferenceException and a plethora of null object checking code.
            /// </summary>
            userRepo = new UserRepository();
            var user = userRepo.GetById(Guid.NewGuid());

            user.IncrementSessionTicket();
            System.Console.WriteLine("The user's name is {0}", user.Name);

            /// <summary>
            /// An interface is said to be fluent if it returns itself from one or more of its methods. This allows clients to 
            /// chain calls together. note that one of the methods of the interface is not fluent—it returns void. If a method 
            /// returns anything but an interface, it is not fluent. Any chaining of methods that a client does will be halted 
            /// by a call to a method that is not fluent.
            /// </summary>
            fluent = new FluentImplementation();

            fluent.DoSomething()
                .DoSomethingElse()
                .DoSomethingElse()
                .DoSomething()
                .ThisMethodIsNotFluent();
        }

        static Swan swan;
        static Context context;
        static IComponent component;
        static IPredicate predicate;
        static DateTester dateTester;
        static IFluentInterface fluent;
        static IUserRepository userRepo;
        static CompositeComponent composite;
        static IExpectedInterface dependency;
    }
}