﻿using System;
using Patterns.ISP.Abstract;
using System.Collections.Generic;

namespace Patterns.ISP.Concrete
{
    public class ReadCaching<TEntity> : IRead<TEntity>
    {
        private TEntity cachedEntity;
        private IEnumerable<TEntity> allCachedEntities;

        public ReadCaching(IRead<TEntity> decorated)
        {
            this.decorated = decorated;
        }

        public TEntity ReadOne(Guid identity)
        {
            if (cachedEntity == null)
            {
                cachedEntity = decorated.ReadOne(identity);
            }
            return cachedEntity;
        }

        public IEnumerable<TEntity> ReadAll()
        {
            if (allCachedEntities == null)
            {
                allCachedEntities = decorated.ReadAll();
            }
            return allCachedEntities;
        }

        private readonly IRead<TEntity> decorated;
    }
}