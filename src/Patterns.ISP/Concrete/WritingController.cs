﻿using Patterns.ISP.Abstract;

namespace Patterns.ISP.Concrete
{
    /// <summary>
    /// Implementers and clients of the reader interface will only have access to the GetTheme method, 
    /// and implementers and clients of the writer interface will have access to both the GetTheme and 
    /// SetTheme methods. Additionally, any implementation of the IUserSettingsWriter interface is 
    /// automatically an implementation of the IUserSettingsReader interface.
    /// </summary>
    public class WritingController
    {
        private readonly IUserSettingsWriter settings;

        public WritingController(IUserSettingsWriter settings)
        {
            this.settings = settings;
        }

        public void SetTheme(string theme)
        {
            if (settings.GetTheme() != theme)
                settings.SetTheme(theme);
        }
    }
}