﻿using Patterns.ISP.Abstract;

namespace Patterns.ISP.Concrete
{
    public class EntitySavedEvent<TEntity> : IEvent
    {
        public EntitySavedEvent(TEntity entity)
        {
            SavedEntity = entity;
        }

        public string Name { get { return "EntitySaved"; } }

        public TEntity SavedEntity { get; set; }
    }
}