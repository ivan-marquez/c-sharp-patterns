﻿using Patterns.ISP.Abstract;

namespace Patterns.ISP.Concrete
{
    public class DeleteConfirmation<TEntity> : IDelete<TEntity>
    {
        public DeleteConfirmation(IDelete<TEntity> decoratedCrud, IUserInteraction userInteraction)
        {
            this.decoratedCrud = decoratedCrud;
            this.userInteraction = userInteraction;
        }

        public void Delete(TEntity entity)
        {
            if (userInteraction.Confirm("Are you sure you want to delete the entity?"))
            {
                decoratedCrud.Delete(entity);
            }
        }

        private readonly IDelete<TEntity> decoratedCrud;
        private readonly IUserInteraction userInteraction;
    }
}