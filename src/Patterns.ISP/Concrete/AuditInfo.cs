﻿using System;

namespace Patterns.ISP.Concrete
{
    public class AuditInfo
    {
        public string UserName { get; set; }

        public DateTime TimeStamp;
    }
}