﻿using System;
using Patterns.ISP.Abstract;

namespace Patterns.ISP.Concrete
{
    public class ConsoleUserInteraction : IUserInteraction
    {
        public bool Confirm(string message)
        {
            Console.WriteLine("{0} [y/N]", message);
            var keyInfo = Console.ReadKey();

            return (keyInfo.Key == ConsoleKey.Y);
        }
    }
}