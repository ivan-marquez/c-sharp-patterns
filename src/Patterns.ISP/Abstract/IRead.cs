﻿using System;
using System.Collections.Generic;

namespace Patterns.ISP.Abstract
{
    public interface IRead<TEntity>
    {
        TEntity ReadOne(Guid identity);

        IEnumerable<TEntity> ReadAll();
    }
}