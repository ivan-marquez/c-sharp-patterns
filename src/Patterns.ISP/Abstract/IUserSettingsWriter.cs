﻿namespace Patterns.ISP.Abstract
{
    public interface IUserSettingsWriter : IUserSettingsReader
    {
        void SetTheme(string theme);
    }
}