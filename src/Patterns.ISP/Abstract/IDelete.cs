﻿namespace Patterns.ISP.Abstract
{
    public interface IDelete<TEntity>
    {
        void Delete(TEntity entity);
    }
}