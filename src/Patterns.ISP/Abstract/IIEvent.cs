﻿namespace Patterns.ISP.Abstract
{
    public interface IEvent
    {
        string Name { get; }
    }
}