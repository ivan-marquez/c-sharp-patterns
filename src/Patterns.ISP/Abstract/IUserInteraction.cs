﻿namespace Patterns.ISP.Abstract
{
    public interface IUserInteraction
    {
        bool Confirm(string message);
    }
}