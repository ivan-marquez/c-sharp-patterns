﻿namespace Patterns.ISP.Abstract
{
    public interface IUserSettingsReader
    {
        string GetTheme();
    }
}