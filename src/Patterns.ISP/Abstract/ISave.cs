﻿namespace Patterns.ISP.Abstract
{
    /// <summary>
    /// The Create and Update methods have identical signatures. Not only that, they serve very similar purposes: the 
    /// former saves a new entity, and the latter saves an existing entity. You could unify these methods into one Save 
    /// method, which acknowledges that the distinction between creating and up-dating is an implementation detail that 
    /// clients don’t need to know about. After all, a client is likely to want to both save and update an entity, so 
    /// requiring two interfaces that are so similar seems needless when there is a viable alternative. All that clients 
    /// of the interface want to do is persist an entity.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface ISave<TEntity>
    {
        void Save(TEntity entity);
    }
}